FROM registry.gitlab.com/alexcandidos/consumo-gasolina-recife:dependencies

COPY . $APP_PATH

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

#ENTRYPOINT ['python', '/usr/src/app/manage.py', 'runserver', '0.0.0.0:8000']
